import Vue from 'vue'
import Router from 'vue-router'
import Register from '@/components/Register'
import Modal from '@/components/Modal'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Register',
      component: Register
    },
    {
      path: '/',
      name: 'Modal',
      component: Modal
    }
  ]
})
